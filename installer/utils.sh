#!/bin/sh

MYDIR=$(dirname "$(readlink -f "$0")")

get_cmu_sw_version()
{
	_ver=$(/bin/grep "^JCI_SW_VER=" /jci/version.ini | /bin/sed 's/^.*_\([^_]*\)\"$/\1/')
	_patch=$(/bin/grep "^JCI_SW_VER_PATCH=" /jci/version.ini | /bin/sed 's/^.*\"\([^\"]*\)\"$/\1/')
	_flavor=$(/bin/grep "^JCI_SW_FLAVOR=" /jci/version.ini | /bin/sed 's/^.*_\([^_]*\)\"$/\1/')

	if [ ! -z "${_flavor}" ]; then
		echo "${_ver}${_patch}-${_flavor}"
	else
		echo "${_ver}${_patch}"
	fi
}

log_message()
{
	echo "$*" 1>&2
	echo "$*" >> "${MYDIR}/AIO_log.txt"
	/bin/fsync "${MYDIR}/AIO_log.txt"
}

show_message()
{
	sleep 5
	killall jci-dialog
#	log_message "= POPUP: $* "
	/jci/tools/jci-dialog --info --title="MESSAGE" --text="$*" --no-cancel &
}

show_message_OK()
{
	sleep 4
	killall jci-dialog
#	log_message "= POPUP: $* "
	/jci/tools/jci-dialog --confirm --title="CONTINUE INSTALLATION?" --text="$*" --ok-label="YES - GO ON" --cancel-label="NO - ABORT"
	if [ $? != 1 ]
		then
			killall jci-dialog
			return
		else
			show_message "INSTALLATION ABORTED! PLEASE UNPLUG USB DRIVE"
			sleep 5
			exit
		fi
}

check_compat()
{
 if [ "${CMU_SW_VER}" = "56.00.100A-ADR" ] \
 || [ "${CMU_SW_VER}" = "56.00.230A-ADR" ] \
 || [ "${CMU_SW_VER}" = "56.00.240B-ADR" ] \
 || [ "${CMU_SW_VER}" = "56.00.511A-ADR" ] \
 || [ "${CMU_SW_VER}" = "56.00.512A-ADR" ] \
 || [ "${CMU_SW_VER}" = "56.00.513C-ADR" ] \
 || [ "${CMU_SW_VER}" = "56.00.230A-EU" ] \
 || [ "${CMU_SW_VER}" = "56.00.511A-EU" ] \
 || [ "${CMU_SW_VER}" = "56.00.512A-EU" ] \
 || [ "${CMU_SW_VER}" = "56.00.513B-EU" ] \
 || [ "${CMU_SW_VER}" = "56.00.513C-EU" ] \
 || [ "${CMU_SW_VER}" = "59.00.331A-EU" ] \
 || [ "${CMU_SW_VER}" = "55.00.650A-NA" ] \
 || [ "${CMU_SW_VER}" = "55.00.753A-NA" ] \
 || [ "${CMU_SW_VER}" = "55.00.760A-NA" ] \
 || [ "${CMU_SW_VER}" = "56.00.521A-NA" ] \
 || [ "${CMU_SW_VER}" = "58.00.250A-NA" ] \
 || [ "${CMU_SW_VER}" = "56.00.401A-JP" ]
  then
    log_message "Detected compatible version ${CMU_SW_VER}"
  else
    show_message_OK "Detected previously unknown version ${CMU_SW_VER}!\n\n To continue anyway choose YES\n To abort choose NO"
    log_message "Detected previously unknown version ${CMU_SW_VER}!"
 fi
}
