#!/bin/sh

# disable watchdog and allow write access
echo 1 > /sys/class/gpio/Watchdog\ Disable/value
mount -o rw,remount /
/jci/bin/jci-log.sh stop

MYDIR=$(dirname "$(readlink -f "$0")")
. ${MYDIR}/utils.sh

CMU_SW_VER=$(get_cmu_sw_version)
rm -f "${MYDIR}/AIO_log.txt"

/jci/tools/jci-dialog --3-button-dialog --title="Tweaks Selection for AR4-opkg" --text="Choosing Method" --ok-label="Install" --cancel-label="Uninstall" --button3-label="Skip"
choice=$?

if [ "$choice" = '0' ]; then
 msg="Install.."
 check_compat
 show_message "INSTALLATION STARTED"

 if [ ! -e /etc/opkg/opkg.conf ]
 then
  log_message "Prepare opkg environment"
  mkdir -p /tmp/mnt/data_persist/dev/opkg
  ln -sf /data_persist/dev/opkg/ /usr/lib/opkg
  mkdir /etc/opkg
  cp ${MYDIR}/config/opkg/opkg.conf /etc/opkg

  log_message "Install autorun AND Upgrade wget"
  log_message "Install basic requirement"
  log_message "`opkg install ${MYDIR}/config/opkg/autorun_1.0_arm.ipk`"
  log_message "`opkg install ${MYDIR}/config/opkg/autorun-plugins-checkonline_0.1_arm.ipk`"
  log_message "`opkg install ${MYDIR}/config/opkg/libuuid_1.0.3_arm.ipk`"
  log_message "`opkg install ${MYDIR}/config/opkg/libressl_2.5.4_arm.ipk`"
  log_message "`opkg install ${MYDIR}/config/opkg/wget_1.19-1_arm.ipk`"

  log_message "Install androidauto/videoplayer/Thai fonts"
  log_message "`opkg install ${MYDIR}/config/opkg/websocketd_0.2.12-1_arm.ipk`"
  log_message "`opkg install ${MYDIR}/config/opkg/libuv_1.13.1_arm.ipk`"
  log_message "`opkg install ${MYDIR}/config/opkg/node_0.12.18_arm.ipk`"
  log_message "`opkg install ${MYDIR}/config/opkg/mzd-disable-watchdog_0.1_arm.ipk`"
  log_message "`opkg install ${MYDIR}/config/opkg/mzd-apps_0.1-1_arm.ipk`"
  log_message "`opkg install ${MYDIR}/config/opkg/mzd-videoplayer_2.9-1_arm.ipk`"
  log_message "`opkg install ${MYDIR}/config/opkg/headunit-izacus_0.94_arm.ipk`"
  log_message "`opkg install ${MYDIR}/config/opkg/mzd-cschatthai-font_0.1_arm.ipk`"

 else
  log_message "OPKG Already configured"
  if [ -e /tmp/root/online.status ]; then
   log_message "`opkg update`"
   log_message "`opkg install headunit-izacus`"
   log_message "`opkg install mzd-videoplayer`"
   log_message "`opkg install mzd-cschatthai-font`"
  else
   log_message "Install basic requirement"
   log_message "Install androidauto/videoplayer/Thai fonts"
   log_message "`opkg install ${MYDIR}/config/opkg/websocketd_0.2.12-1_arm.ipk`"
   log_message "`opkg install ${MYDIR}/config/opkg/libuv_1.13.1_arm.ipk`"
   log_message "`opkg install ${MYDIR}/config/opkg/node_0.12.18_arm.ipk`"
   log_message "`opkg install ${MYDIR}/config/opkg/mzd-disable-watchdog_0.1_arm.ipk`"
   log_message "`opkg install ${MYDIR}/config/opkg/mzd-apps_0.1-1_arm.ipk`"
   log_message "`opkg install ${MYDIR}/config/opkg/mzd-videoplayer_2.9-1_arm.ipk`"
   log_message "`opkg install ${MYDIR}/config/opkg/headunit-izacus_0.94_arm.ipk`"
   log_message "`opkg install ${MYDIR}/config/opkg/mzd-cschatthai-font_0.1_arm.ipk`"
 fi
elif [ "$choice" = '1' ]; then
  msg="Uninstall.."
 list=`opkg list_installed|awk '{print $1}'`
 for l in $list
 do
  opkg remove --force-removal-of-dependent-packages $l
 done 
 opkg remove autorun
 rm -rf /tmp/data_persist/dev/opkg
 rm -f /usr/lib/opkg
 rm -rf /etc/opkg
 killall jci-dialog
 reboot
else
 msg="Abort.."
 show_message "INSTALLATION ABORTED PLEASE UNPLUG USB DRIVE"
fi

