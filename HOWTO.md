#This guide for AIO users only. 

This script incompatible with AIO. User should uninstall all tweaks before.

#Install 
1. download zip file from https://gitlab.com/mzdonline/AR4. Select download icons (right of History button) .
2. extract AR4-xxx.zip 
3. copy all file in installer/* to Root USB 
4. Move to car, plug USB for install
5. Choose Install.
6. Reboot and complete install.

#Uninstall
1. Using same file. No need to new copy. 
2. choose Uninstall.
